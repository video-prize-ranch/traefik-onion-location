package traefik_onion_location_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"codeberg.org/video-prize-ranch/traefik-onion-location"
)

func TestOnionLocation(t *testing.T) {
	cfg := traefik_onion_location.CreateConfig()
	cfg.Onion = "http://example.onion"

	ctx := context.Background()
	next := http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {})

	handler, err := traefik_onion_location.New(ctx, next, cfg, "onion-location")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://localhost/this/is/a/path", nil)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(recorder, req)

	assertHeader(t, req, "Onion-Location", "http://example.onion/this/is/a/path")
}

func assertHeader(t *testing.T, req *http.Request, key, expected string) {
	t.Helper()

	if req.Header.Get(key) != expected {
		t.Errorf("invalid header value: %s", req.Header.Get(key))
	}
}
