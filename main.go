package traefik_onion_location

import (
	"context"
	"fmt"
	"net/http"
)

// Config the plugin configuration.
type Config struct {
	Onion string `json:"onion,omitempty"`
}

// CreateConfig creates the default plugin configuration.
func CreateConfig() *Config {
	return &Config{
		Onion: "",
	}
}

type OnionLocation struct {
	next     http.Handler
	onion 	 string
	name     string
}

// New created a new Demo plugin.
func New(ctx context.Context, next http.Handler, config *Config, name string) (http.Handler, error) {
	if config.Onion == "" {
		return nil, fmt.Errorf("onion cannot be empty")
	}

	return &OnionLocation{
		onion:  config.Onion,
		next:     next,
		name:     name,
	}, nil
}

func (a *OnionLocation) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	rw.Header().Add("Onion-Location", a.onion + req.URL.Path)

	a.next.ServeHTTP(rw, req)
}